﻿using System;

using System.Diagnostics;
using System.Management;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Timers;

namespace AntiCheat
{
    public class Processo
    {
        public string process { get; set; }
        public override string ToString()
        {
            return process;
        }
    }

    public static class Global
    {
        public static List<string> Blacklist { get; set; }
    }
    public class Program
    {
        
        static void Main(string[] args)
        {

            ProcessMonitor ProcessMonitor = new ProcessMonitor();

            List<string> blacklist = ProcessMonitor.getBlacklist();
            Global.Blacklist = blacklist;
            Timer updateTimer = new Timer();
            updateTimer.Elapsed += new ElapsedEventHandler(updateBlacklist);
            updateTimer.Interval = 60000 * 10;
            updateTimer.Enabled = true;
            void updateBlacklist(object source, ElapsedEventArgs e) {
                List<string> blacklist = ProcessMonitor.getBlacklist();
                Global.Blacklist = blacklist;
                Console.WriteLine("Blacklist Updated");
            }

            Console.WriteLine("Checking current processes...");
            ProcessMonitor.RunningProcesses();


            Console.WriteLine("Starting watch for new processes...");
            ProcessMonitor.WaitForProcess();
        }


    }

    public class ProcessMonitor
    {

        

        public static List<string> getBlacklist()
        {
            List<Processo> processes;
            using (var client = new HttpClient())
            {
                
                client.BaseAddress = new Uri("https://processlist-gameguard-project.herokuapp.com/");
                client.DefaultRequestHeaders.Add("User-Agent", "Anything");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync("process").Result;
                
                if(!response.IsSuccessStatusCode){
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("Erro ao comunicar com a api!");
                    Console.ResetColor();
                    Console.WriteLine();

                    ProcessMonitor p = new ProcessMonitor();
                    p.killGameProcess();
                }
                
                processes = response.Content.ReadAsAsync<List<Processo>>().Result;
            }
            var blacklist = new List<string>();
            foreach (Processo p in processes)
            {
                blacklist.Add(p.process);
            }
            return blacklist;
        }

        public void RunningProcesses()
        {
            Process[] runningProcesses = Process.GetProcesses();

            List<string> runningProcessesNames = new List<string>();
            foreach (Process p in runningProcesses)
            {
                runningProcessesNames.Add(p.ProcessName);
                //Console.WriteLine(p.ProcessName);
            }

            var matches = runningProcessesNames.Intersect(Global.Blacklist);
            foreach(string match in matches)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0} is running!", match);
                Console.ResetColor();
                Console.WriteLine();

                killProcessByName(match);
                killGameProcess();
            }

        }


        public void WaitForProcess()
        {

            try 
            {
                string queryString = "SELECT * FROM __InstanceCreationEvent WITHIN .025 WHERE TargetInstance ISA 'Win32_Process'";
                ManagementEventWatcher managementEventWatcher = new ManagementEventWatcher(@"\\.\root\CIMV2", queryString);
                managementEventWatcher.EventArrived += new EventArrivedEventHandler(ProcessStartEventArrived);

                managementEventWatcher.Start();

                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                managementEventWatcher.Stop();

            }
            catch(Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
            
        }
        void ProcessStartEventArrived(object sender, EventArrivedEventArgs e)
        {
            ManagementBaseObject targetInstance = (ManagementBaseObject)e.NewEvent.Properties["TargetInstance"].Value;
            string process = (string)targetInstance.Properties["Name"].Value; //processo.exe
            string processName = process.Substring(0, process.LastIndexOf('.'));// remove extensao

            //Console.WriteLine("{0}   -   {1}", processName, process);
            if (Global.Blacklist.Contains(processName))
            {
                //Writes in Color
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0} just started!", processName);
                Console.ResetColor();
                Console.WriteLine();

                killProcessByName(processName);
                //kills game when hack is launched
                killGameProcess();
            }

            //kills game if hack is already open
            if(processName.Equals("hl2")){
                bool isSafe = true;
                Process[] runningProcesses = Process.GetProcesses();

                List<string> runningProcessesNames = new List<string>();
                foreach (Process p in runningProcesses)
                {
                    runningProcessesNames.Add(p.ProcessName);
                }

                var matches = runningProcessesNames.Intersect(Global.Blacklist);
                foreach(string match in matches)
                {
                    isSafe = false;
                }

                if(!isSafe){
                    killGameProcess();
                }
            }
        }
        public void killGameProcess(){
            foreach (var p in Process.GetProcessesByName("hl2"))
                {
                    p.Kill();
                    //Writes in Color
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("Game process killed because of suspicious activity");
                    Console.ResetColor();
                    Console.WriteLine();
                }
        }
        public void killProcessByName(string processName){
            foreach (var p in Process.GetProcessesByName(processName))
                {
                    p.Kill();
                    //Writes in Color
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("{0} process killed because of suspicious activity", processName);
                    Console.ResetColor();
                    Console.WriteLine();
                }
        }
    }
}

