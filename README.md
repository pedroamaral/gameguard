# GameGuard

This project is a crude replication of the anticheat techniques found in the following article:

GameGuard: A Windows-based Software Architecture for Protecting Online Games against Hackers. 

Available at: https://dl.acm.org/doi/10.1145/1852611.1852643

## Running

To test this project, run the following executable on a windows machine.

```
bin/Debug/netcoreapp3.1/C.exe
```